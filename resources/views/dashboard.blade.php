@extends('layouts.app')

@section('title', 'Dashboard | My First Laravel Project')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div>ID: {{$id}}</div>
                    <div>Email: {{$email}}</div>
                    You are logged in!
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Todos</div>

                <div class="card-body">
                  <ol class="">
                    @foreach ($user_todos as $user_todo)
                        <li>
                            <strong class="text-uppercase">{{$user_todo->name}}</strong>
                            <ul>
                                @foreach ($entries as $entry)
                                    @foreach ($entry as $single_entry)
                                        @if($single_entry->todo_id == $user_todo->id)
                                            <li>
                                                {{$single_entry->entry}} - {{$single_entry->status}}
                                            </li>
                                        @endif
                                    @endforeach
                                @endforeach
                            </ul>
                        </li>
                    @endforeach
                </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add Todo</div>

                <div class="card-body">
                
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Todo</div>

                <div class="card-body">
                
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Show Todo</div>

                <div class="card-body">
                
                </div>
            </div>
        </div>
    </div>

@endsection
