<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <link href="{{ asset('css/app.css') }}" media="all" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/styles.css') }}" media="all" rel="stylesheet" type="text/css" />

    </head>
    <body>

        <header>
            <nav>
                <div class="container">
                    <div class="logo">
                        <a href="/">LOGO</a>
                    </div>
                    <div class="navigation">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href="/about/">About</a></li>
                            <li><a href="/login">Login</a></li>
                        </ul>
                    </div>
                </div>

                <div class="clear"></div>
            </nav>
        </header>

        @section('sidebar')
        @show

        <div class="banner">
            @yield('banner')
        </div>
        

        <div class="container">
            @yield('content')
        </div>



        <footer>
            
        </footer>
       
    </body>
</html>
