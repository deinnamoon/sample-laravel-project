@extends('layouts.app')

@section('title', 'My First Laravel Project')

@section('sidebar')
    @parent
@endsection


@section('content')

    <h1>My First Laravel Todo App Project</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. <a href="#">Ut enim ad minim</a> veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

@endsection
