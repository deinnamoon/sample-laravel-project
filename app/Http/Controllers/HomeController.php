<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Todo;
use App\TodoEntry;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    public function index() {
        return view('index');
    }

    public function about() {
        $words = array('This', 'is', 'hello', 'world');
        return view('about')->with('words', $words);
    }

    public function dashboard()
    {
        $id = Auth::user()->id;
        $email = Auth::user()->email;

        $user_todos = User::find($id)->Todo;
        $entries = array();

        foreach ($user_todos as $user_todo) {
            $entries[] = $user_todo->TodoEntry;
        }

        return view('dashboard')
                    ->with('id', $id)
                    ->with('email', $email)
                    ->with('user_todos', $user_todos)
                    ->with('entries', $entries);
    }

    

}
