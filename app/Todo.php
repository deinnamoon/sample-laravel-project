<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{

	public function todoEntry() {
    	return $this->hasMany(TodoEntry::class);
    }

	public function user() {
		return $this->belongsTo(User::class, 'foreign_key');
	}

}
