@extends ('layouts.app')
@section('title', 'Create Todo')

@section('content')

    <h1>Create Todo</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>


	<form>
		<div class="form-group row">
			<label for="todo-title" class="col-sm-2 col-form-label">Title</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="todo-title" value="">
			</div>
		</div>
		<div class="form-group row">
			<label for="todo-description" class="col-sm-2 col-form-label">Description</label>
			<div class="col-sm-10">
				<textarea class="form-control" id="todo-description" value=""></textarea>
			</div>
		</div>
		<hr>
		<div class="form-group row">
			<label for="todo-items" class="col-sm-2 col-form-label">Todo Items</label>
			<div class="col-sm-10 todo-item">
				<input type="text" class="form-control" id="todo-items" value="">
			</div>
			<div class="col-sm-12 text-right">
				<button type="button" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i></button>
			</div>
		</div>

		<button type="submit" class="btn btn-primary">Submit</button>
	</form>
    

@endsection