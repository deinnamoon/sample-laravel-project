<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('about', 'HomeController@about')->name('about');
Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');


Route::get('/todo', 'TodoController@index')->name('todo-index');
Route::get('/todo/create', 'TodoController@create')->name('todo-create');
