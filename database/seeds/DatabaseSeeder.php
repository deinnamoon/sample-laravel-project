<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('secret'),
        ]);

        factory(App\User::class, 50)->create()->each(function ($u) {
	        $u->posts()->save(factory(App\Post::class)->make());
	    });

	    $this->call([
	        UsersTableSeeder::class,
	        PostsTableSeeder::class,
	        CommentsTableSeeder::class,
	    ]);
    }
}
