<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TodoEntry extends Model
{
    public function todo() {
    	return $this->belongsTo(Todo::class, 'foreign_key');
    }
}
